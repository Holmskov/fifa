<?php
session_start();

if(!isset($_SESSION['loggedin'])) {
    header("Location: login.php");
}

include("includes/header.php");
require_once("includes/db.php");
require_once("includes/functions.php");

$user = $_SESSION['loggedin'];

//inserts new player to the table Users
if (isset($_POST['nySpiller'])) {
    $nySpiller = $_POST["nySpiller"];

//inserts new player to the table Users
    $sql = "INSERT INTO Users (Navn)
VALUES ('$nySpiller')";
    mysqli_query($conn, $sql);
}

if (isset($_POST['deletePlayer'])) {
    $deletePlayer = $_POST["deletePlayer"];
//Deletes the chosen player from the table Users
    $sql = "DELETE FROM Users
WHERE id = $deletePlayer";
    mysqli_query($conn, $sql);

}
?>
<body>

<div class="jumbotron text-center">
    <h1 class="forside">FIFA LEADERBOARD</h1>
    <div class="company"><?php companyName(); ?>  </div>
    <div class="buttonCenter">
            <a href="includes/logout.php" class="btn btn-info">Log ud</a>
        <button type="button" class="btn btn-info nySpiller" data-toggle="modal" data-target="#exampleModal" data-whatever="@mdo">Ny spiller</button>
        <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title" id="exampleModalLabel">Ny spiller</h4>
                    </div>
                    <div class="modal-body">
                        <form method="post">
                            <input type="text" required name="nySpiller" placeholder="Ny spiller" required class="form-control"><br>
                            <input type="submit" value="Tilmeld" class="btn btn-info">
                        </form>
                    </div>
                </div>
            </div>
        </div>

        <button type="button" class="btn btn-info nySpiller" data-toggle="modal" data-target="#exampleModal2" data-whatever="@mdo">Slet spiller</button>
        <div class="modal fade" id="exampleModal2" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title" id="exampleModalLabel">Slet spiller</h4>
                    </div>
                    <div class="modal-body">
                        <form method="post">
                            <select name="deletePlayer" class="form-control">
                                <option  disabled selected hidden>Vælg spiller</option>
                                <?php select(); ?>
                            </select>
                            <input type="submit" value="Slet" class="btn btn-info deleteBtn">
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="container">
    <div class="row">
        <div class="tabelDiv">
        <h3 class="dropdown">Tabel</h3>
        <table id="stilling" class="tablesorter table table-striped table-bordered tableButton" cellspacing="0" width="100%">
            <thead class="theadClass">
            <tr>
                <th>#</th>
                <th>Navn</th>
                <th>K</th>
                <th>V</th>
                <th>U</th>
                <th>T</th>
                <th>P</th>
                <th>%</th>
                <th>point pr kamp</th>
            </tr>
            </thead>
            <tbody>
            <?php table(); ?>
            </tbody>
        </table>
            </br>
        <a href="includes/tabel.php" class="btn btn-info tabelNyt">Nyt vindue</a>
        </div>
        <div class="kampDiv">
        <h3 class="dropdown">Kamp information</h3>
        <p class="dropdown">Indtast kampens oplysninger</p>
        <form action="includes/fifa2.php" method="post">
            <div class="col-md-6">
                <p class="dropdown">Hold 1</p>
            <select name="player1" class="form-control">
                <option  disabled selected hidden>Vælg spiller</option>
                    <?php select(); ?>
            </select>
                <select name="player1b" class="form-control">
                    <option  disabled selected hidden>Vælg spiller</option>
                   <?php select(); ?>
                </select>
            <input type="text" required name="score1" placeholder="Resultat" required class="form-control"><br>
            </div>
        <div class="col-md-6">
            <p class="dropdown">Hold 2</p>
            <select name="player2" class="form-control">
                <option  disabled selected hidden>Vælg spiller</option>
                    <?php select(); ?>
                </select>
            <select name="player2b" class="form-control">
                <option  disabled selected hidden>Vælg spiller</option>
                <?php select(); ?>
            </select>
                <input type="number" required name="score2" placeholder="Resultat" required class="form-control"><br>
                  </div>
                <div class="buttonContainer">
                <input type="submit" value="GO!" class="btn btn-info">
                </div>
            </form>
        </br>
        </br>
        </br>
        </div>
        </div>
        </div>

<footer class="footer navbar-fixed-bottom footerBar">
    <div class="container">
        <p>WeCode fifa leaderboard - All right reserved.</p>
    </div>
</footer>
</body>
</html>