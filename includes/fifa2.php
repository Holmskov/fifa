<?php
session_start();

if(!isset($_SESSION['loggedin'])) {
    header("Location: ../login.php");
}

include("header.php");
require_once("db.php");
require_once("functions.php");

    $spiller1 = $_POST["player1"];
    $spiller2 = $_POST["player2"];
    if(isset($_POST["player1b"])){
        $spiller3 = $_POST["player1b"];
    }
    if(isset($_POST["player2b"])){
        $spiller4 = $_POST["player2b"];
    }
    $score1 = $_POST["score1"];
    $score2 = $_POST["score2"];


if (isset($spiller3, $spiller4)){
    $sql ="INSERT INTO scores (Score1, Score2, Spiller1, Spiller2, Spiller3, Spiller4)
    VALUES('$score1', '$score2', '$spiller1', '$spiller2', '$spiller3', '$spiller4')";
} else {
    $sql = "INSERT INTO scores (Score1, Score2, Spiller1, Spiller2)
VALUES ('$score1', '$score2', '$spiller1', '$spiller2')
";
}
    mysqli_query($conn, $sql);


    $sql = "SELECT host1.Navn as spiller1, host2.Navn as spiller3, opponent1.Navn as spiller2, opponent2.Navn as spiller4, scores.Score1, scores.Score2, scores.currentTime
            FROM scores
            INNER JOIN Users host1
            ON scores.Spiller1=host1.ID
            LEFT JOIN Users host2
            ON scores.Spiller3=host2.ID
            INNER JOIN Users opponent1
            ON scores.Spiller2=opponent1.ID
            LEFT JOIN Users opponent2
            ON scores.Spiller4=opponent2.ID
            ORDER BY `scores`.`currentTime`  DESC
            LIMIT 10";

    $result = mysqli_query($conn, $sql);
    $output = '';
    if ($result->num_rows > 0) {
        // output data of each row
        while ($row = $result->fetch_assoc()) {
            $output .= $row["currentTime"] . ": " . $row["spiller1"] . " " . $row["spiller3"] . "  " . $row["Score1"] . " - " . $row["Score2"] . " " . $row["spiller2"] . " " . $row["spiller4"] . "<br>";
        }
    } else {
        echo "ingen resultater";
    }

if (isset($spiller3, $spiller4)){
    $sql = "UPDATE Users SET Kampe = Kampe + 1 WHERE id IN ($spiller1, $spiller3, $spiller2, $spiller4)";
    mysqli_query($conn, $sql);

    if ($score1 > $score2) {
        $sql = "UPDATE Users SET 
                  Vundet  = IF(id=$spiller2, Vundet, IF(id=$spiller4, Vundet, Vundet + 1)),
                  Tabt    = IF(id=$spiller3, Tabt, IF(id=$spiller1, Tabt, Tabt + 1)),
                  Point   = IF(id=$spiller2, Point, IF(id=$spiller4, Point, Point + 3)) WHERE ID IN ($spiller1,$spiller2,$spiller3,$spiller4)";
        mysqli_query($conn, $sql);

    } elseif ($score1 < $score2) {
        $sql = "UPDATE Users SET 
                  Vundet  = IF(id=$spiller3, Vundet, IF(id=$spiller1, Vundet, Vundet + 1)),
                  Tabt    = IF(id=$spiller2, Tabt, IF(id=$spiller4, Tabt, Tabt + 1)),
                  Point   = IF(id=$spiller3, Point, IF(id=$spiller1, Point, Point + 3)) WHERE ID IN ($spiller1,$spiller2,$spiller3,$spiller4)";
        mysqli_query($conn, $sql);
    } elseif ($score1 === $score2) {
        $sql = "UPDATE Users SET 
                  Uafgjort = IF(!id=$spiller1 AND $spiller2 AND $spiller3 AND $spiller4, Uafgjort, Uafgjort + 1),
                  Point    = IF(!id=$spiller1 AND $spiller2 AND $spiller3 AND $spiller4, Point, Point + 1) WHERE ID IN ($spiller1,$spiller2,$spiller3,$spiller4)";
        mysqli_query($conn, $sql);
    }
} elseif (!isset($spiller3, $spiller4)) {
    $sql = "UPDATE Users SET Kampe = Kampe + 1 WHERE id IN ($spiller1, $spiller2)";
    mysqli_query($conn, $sql);

    if ($score1 > $score2) {
        $sql = "UPDATE Users SET
                  Vundet = IF(id=$spiller2, Vundet, Vundet + 1),
                  Tabt   = IF(id=$spiller1, Tabt, Tabt + 1),
                  Point  = IF(id=$spiller2, Point, Point + 3) WHERE ID IN($spiller1,$spiller2)";
        mysqli_query($conn, $sql);
    } elseif ($score1 < $score2) {
        $sql = "UPDATE Users SET
                  Vundet = IF(id=$spiller1, Vundet, Vundet + 1),
                  Tabt   = IF(id=$spiller2, Tabt, Tabt + 1),
                  Point  = IF(id=$spiller1, Point, Point + 3) WHERE ID IN ($spiller1,$spiller2)";
        mysqli_query($conn, $sql);
    } elseif ($score1 === $score2) {
        $sql = "UPDATE Users SET 
                  Uafgjort = IF(!id=$spiller1 AND $spiller2, Uafgjort, Uafgjort + 1),
                  Point    = IF(!id=$spiller1 AND $spiller2, Point, Point + 1) WHERE ID IN ($spiller1,$spiller2)";
        mysqli_query($conn, $sql);

    }
}


//Update table Users with percentage of games played
$sql = "UPDATE Users SET Procent=(Vundet/Kampe)*100";
    mysqli_query($conn, $sql);

?>
<body>

<div class="jumbotron text-center">
    <h1 class="forside">FIFA LEADERBOARD</h1>
    <!--Put in your company name here-->
    <p class="forside">WeCode</p>
    <form method="get" action="logUd.php">
        <div class="buttonContainer">
            <button type="submit" class="btn btn-info ">Log ud</button>
        </div>
    </form>
</div>

<div class="container">
    <div class="row">
        <div class="col-md-6 senestSpillede">
            <h3 class="dropdown">Seneste spillede kampe</h3>
            <p class="dropdown">Her kan du se de senest spillede kampe samt resultatet af kampen.</p>
            <div id="kampe">
            <?php echo $output; ?>
            </div>
        </div>

        <div class="col-md-6">
            <h3 class="dropdown">Kamp information</h3>
            <p class="dropdown">Her kan du se en tabel over kampene</p>
            <table id="stilling" class="tablesorter table table-striped table-bordered" cellspacing="0" width="100%">
                <thead>
                <tr>
                    <th>#</th>
                    <th>Navn</th>
                    <th>K</th>
                    <th>V</th>
                    <th>U</th>
                    <th>T</th>
                    <th>P</th>
                    <th>%</th>
                </tr>
                </thead>
                <tbody>
                <?php table();?>
                        </tbody>
                      </table>
            <div class="buttonRight">
            <form id="truncate2" method="post" onsubmit="return confirm('Du er igang med at slette data, er du sikker på du vil dette?');">
            <button type="submit" class="btn btn-info btnTruncate" name="truncate2">Nulstil</button>
                </form>
            </div>
            </div>

        </div>
        <div class="row">
            <form method="get" action="../index.php">
            <div class="buttonContainer">
                <button type="submit" class="btn btn-info btnBack">Tilbage</button>
            </div>
            </form>
        </div>
    </div>

</body>
</html>