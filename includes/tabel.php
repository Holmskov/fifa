<?php
session_start();
require_once("db.php");
require_once("functions.php");

?>
<!DOCTYPE html>
<html lang="en">
<?php include("header.php") ?>
<body>

<div class="jumbotron text-center">
    <h1 class="forside">FIFA LEADERBOARD</h1>
</div>
<div class="container">
<div class="row">
        <h3 class="dropdown">Tabel</h3>
        <table id="stilling" class="tablesorter table table-striped table-bordered tableButton" cellspacing="0" width="100%">
            <thead class="theadClass">
            <tr>
                <th>#</th>
                <th>Navn</th>
                <th>K</th>
                <th>V</th>
                <th>U</th>
                <th>T</th>
                <th>P</th>
                <th>%</th>
                <th>point pr kamp</th>
            </tr>
            </thead>
            <tbody>
            <?php table(); ?>
            </tbody>
        </table>
    </br>
    </br>
    <iframe id="start" name="start" src="http://tunein.com/embed/player/s24861/?autoplay=true" style="width:100%;height:100px;" scrolling="no" frameborder="no"></iframe>

    <a href="../index.php" class="btn btn-lg btn-block btn-info tabel">Tilbage</a>
    <br/>

</div>
</div>

<footer class="footer navbar-fixed-bottom footerBar">
    <div class="container">
        <p>WeCode fifa leaderboard - All right reserved.</p>
    </div>
</footer>
</body>
</html>