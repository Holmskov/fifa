<?php include("includes/header.php") ?>
<body>

<div class="jumbotron text-center">
    <h1 class="forside">FIFA LEADERBOARD</h1>
</div>
<div class="container">
    <div class="row">
        <form class="form-signup" action="includes/signup.php" method="post">
            <h2 class="form-signin-heading">Opret bruger</h2>
            <input class="form-control" type="text" name="first" placeholder="Fornavn" required>
            <input class="form-control" type="text" name="last" placeholder="Efternavn" required>
            <input class="form-control" type="text" name="username" placeholder="Brugernavn" required>
            <input class="form-control" type="password" name="password" placeholder="Kodeord" required>
            <button class="btn btn-lg btn-block btn-info" type="submit">Registrer</button>
            <a href="login.php" class="btn btn-lg btn-block btn-info">Tilbage</a>
        </form>

    </div>
</div>

<footer class="footer navbar-fixed-bottom footerBar">
    <div class="container">
        <p>WeCode fifa leaderboard - All right reserved.</p>
    </div>
</footer>
</body>
</html>